# SpeakerAware
> Works only when the device is charging

## Dev
Base dev app:
+ `npm run start`
+ `npm run android`

## Prod
Pre-deploy:
+ `cd android/app`
+ `keytool -genkeypair -v -keystore my-release-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000`

Test prod build:
+ `npx react-native run-android --variant=release`

Deploying production apk:
+ `cd android`
+ `./gradlew -PMYAPP_RELEASE_STORE_PASSWORD=**** -PMYAPP_RELEASE_KEY_PASSWORD=**** assembleRelease`
