import React, {Component} from 'react';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';

export default class ButtonToggler extends Component {
  constructor(props) {
    super(props);

    this.state = {on: false};
    this.handlePress = this.handlePress.bind(this);
  }

  static getDerivedStateFromProps(props, current_state) {
    if (current_state.value !== props.defaultOn) {
      return {
        on: props.defaultOn,
      };
    }
    return null;
  }

  handlePress() {
    this.setState(state => ({
      on: !state.on,
    }));

    if (this.props.onToggle) {
      this.props.onToggle(!this.state.on);
    }
  }

  render() {
    const containerBg = {
      backgroundColor: this.state.on === false ? '#2f95dc' : '#cd5c5c',
    };

    return (
      <TouchableOpacity onPress={this.handlePress}>
        <View style={[styles.container, containerBg]}>
          <Text style={styles.text}>
            {this.state.on === false ? 'ĮJUNGTI' : 'IŠJUNGTI'}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    padding: 20,
  },
  text: {
    color: 'white',
    fontSize: 20,
  },
});
