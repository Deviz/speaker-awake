import React from 'react';
import {
  Platform,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Sound from 'react-native-sound';
import BackgroundTimer from 'react-native-background-timer';
import Slider from '@react-native-community/slider';
import AsyncStorage from '@react-native-community/async-storage';
import VIForegroundService from '@voximplant/react-native-foreground-service';

import ButtonToggler from './components/ButtonToggler';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOn: false,
      //Default repeat set for every 10 minutes
      repeat: 10 * 1000 * 60,
      //20kHz, -3 dBFS, 0.01s tone
      sound: new Sound('tone.wav', Sound.MAIN_BUNDLE, error => {
        if (error) {
          console.log(error);
        }
      }),
    };
  }

  componentDidMount() {
    this._retrieveData();
  }

  async _startService() {
    if (Platform.Version >= 26) {
      const channelConfig = {
        id: 'SpeakerAwareForegroundServiceChannel',
        name: 'SpeakerAware',
        description: 'Programa veikia...',
        enableVibration: false,
        importance: 5,
      };
      await VIForegroundService.createNotificationChannel(channelConfig);
    }
    const notificationConfig = {
      id: 3456,
      title: 'SpeakerAware',
      text: 'Programa veikia...',
      icon: 'ic_notification',
      priority: 2,
    };
    if (Platform.Version >= 26) {
      notificationConfig.channelId = 'SpeakerAwareForegroundServiceChannel';
    }
    await VIForegroundService.startService(notificationConfig);
  }

  async _stopService() {
    await VIForegroundService.stopService();
  }

  _storeData() {
    try {
      AsyncStorage.setItem(
        'SPEAKER_AWARE',
        JSON.stringify({
          repeat: this.state.repeat,
          isRunning: !this.state.isOn,
        }),
      );
    } catch (error) {
      console.log(error);
    }
  }

  _retrieveData() {
    try {
      AsyncStorage.getItem('SPEAKER_AWARE').then(data => {
        if (data !== null) {
          let parsedData = JSON.parse(data);
          this.setState({
            repeat: parsedData.repeat,
            isOn: parsedData.isRunning,
          });
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  startPlaying() {
    this._storeData();
    this._startService();

    BackgroundTimer.runBackgroundTimer(() => {
      //TODO: check if audio is playing already
      this.state.sound.play();
    }, this.state.repeat);
  }

  stopPlaying() {
    BackgroundTimer.stopBackgroundTimer();
    this._stopService();
    this._storeData();
  }

  toMilliSeconds(minutes) {
    return minutes * 1000 * 60;
  }

  toMinutes(milliSeconds) {
    return Math.round((milliSeconds / 60000) * 100) / 100;
  }

  render() {
    return (
      <SafeAreaView>
        <ScrollView style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>SpeakerAware</Text>
              <Text style={styles.sectionDescription}>
                Paspaudus <Text style={styles.highlight}>ĮJUNGTI </Text>
                garsiakalbis taps pastoviai aktyvus
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <ButtonToggler
                defaultOn={this.state.isOn}
                onToggle={isOn => {
                  this.setState({isOn: isOn});
                  isOn ? this.startPlaying() : this.stopPlaying();
                }}
              />
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionDescription}>
                Laikmačio vertė (minutėmis): {this.toMinutes(this.state.repeat)}
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Slider
                disabled={this.state.isOn}
                value={this.toMinutes(this.state.repeat)}
                minimumValue={1}
                maximumValue={15}
                step={1}
                onValueChange={value => {
                  this.setState({
                    repeat: this.toMilliSeconds(value),
                  });
                }}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
    textAlign: 'center',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: 'bold',
  },
});
